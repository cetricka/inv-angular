import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { HotData } from '../HotData';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private dataUrl = 'https://data.konectify.co.ke/data'
  private columnUrl = 'https://data.konectify.co.ke/data/columns'

  constructor(private http:HttpClient) { }

  getData(): Observable<HotData[]> {
    return this.http.get<HotData[]>(this.dataUrl)
  }

  getTitle(): Observable<string[]> {
    return this.http.get<string[]>(this.columnUrl)
  }
}
