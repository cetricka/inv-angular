export interface HotData {
    budget: number;
    genres: string;
    homepage: string;
    id?: string;
    keywords: string;
    original_language: string;
    original_title: string;
    overview: string;
    popularity: string;
    production_companies: string;
    production_countries: number;
    release_date: string;
    revenue: string;
    runtime: string;
    spoken_languages: string;
    status: string;
    tagline: string;
    title: string;
    vote_average: string;
    vote_count: string
}