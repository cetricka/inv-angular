import { Component } from '@angular/core';
import { HotData } from './HotData';
import { DataService } from './services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  data: HotData[] = []
  heads: string[] = []

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.dataService.getData().subscribe((data) => this.data = data);
    this.dataService.getTitle().subscribe((heads) => this.heads = heads);
  }

  id = 'rt';
  settings = {
    rowHeaders: true,
    height: 'auto',
    manualColumnResize: true,
    manualColumnMove: true,
    licenseKey: "non-commercial-and-evaluation",
    minSpareRows: 1,
    persistentState: true
  }
  
}